<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            			<header class="entry-header">
            						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
            						<div class="entry-thumbnail">
            							<?php the_post_thumbnail(); ?>
            						</div>
            						<?php endif; ?>
            
            						<h1 class="entry-title"><?php the_title(); ?></h1>
            			</header>
            			<!-- .entry-header -->
            			<div class="entry-content">
            						<?php the_content(); ?>
            						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'tuannguyen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
            					</div>
            			<!-- .entry-content -->
            
            			<footer class="entry-meta">
            						<?php edit_post_link( __( 'Edit', 'tuannguyen' ), '<span class="edit-link">', '</span>' ); ?>
            					</footer>
            			<!-- .entry-meta -->
        		</article>
  		  <!-- #post -->
          <?php 
          //comments_template(); 
          ?>
  		<?php endwhile; ?>
        <form method="post" action="<?php the_permalink();?>"> 
              <?php  
              if(isset($_POST['category'])){
                  $_SESSION['category'] = $_POST['category'];
              }elseif (!isset($_SESSION['category'])){
              	$_SESSION['category'] = 0;
              }
              
              $args = array(
                  'show_option_all' => "All",
                  'taxonomy'        => 'category',
                  'name'          	=> 'category',
                  'selected' => $_SESSION['category'],
              );
              //wp_dropdown_categories($args);
              tuannguyen_dropdown_categories(array('name'=> 'category','selected' => $_SESSION['category']));
              ?>
              <input type="submit" />	
        				<?php 
        				$args = array(
                            'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1,
                            'showposts' => 9,
        				    //'posts_per_page' => 9,
        				    'post_type' => 'benefit',
        				   // 'orderby' => 'rand',
                            //'tax_query' => array('taxonomy'=> 'category','terms' => 'health','field'=>'slug')
        				);
                        if($_SESSION['category']){
                          $args['cat'] = $_SESSION['category'];
                        }
        				$new_query = new WP_Query($args );
        				if ( $new_query->have_posts() ) : //Kiểm tra xem biến $wp_query của WordPress có giá trị chưa, mặc định hàm này sẽ kiểm tra query trong $wp_query.
        				?>
            				<table>
            				  <thead>
            				  <tr>
            					<th>Support</th>
            					<th>Date</th>
            					<th>Titre/Emission</th>
            					</tr></thead><tbody>
            				<?php
            				while( $new_query->have_posts() ) : //kiểm tra số lần lặp dựa trên số lượng bài viết được thực thi bởi $wp_query.
            				        $new_query->the_post(); // Gọi hàm này để sử dụng những Template Tags.
            				?>
                        				<tr>
                        					<td><?php the_content(); //hiển thị nội dung của post. ?></td>
                        					<td><?php echo get_post_meta(get_the_ID(),'custom_date',true); //echo get_field('custom_date', get_the_ID()) ;?></td>
                        					<td><?php the_title(); //Hiển thị tiêu đề của post, sử dụng get_the_title() để return giá trị của nó. ?></td>
                        				</tr>
            				<?php 
            				endwhile;
            				//wp_reset_postdata();
            				?>
                            </tbody></table>
        			       <div class="row page-navigation"> 
                            	 <?php next_posts_link('&laquo; Older Entries', $new_query->max_num_pages) ?> 
                            	 <?php previous_posts_link('Newer Entries &raquo;') ?> 
                          </div>
                        <?php
        				else : //Nếu $wp_query không có nội dung
        				
        				  echo "Không có bài nào cả";
        				endif;
        				?>
        </form>
	</div>
	<!-- #content -->
</div>
<!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>