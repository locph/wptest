<?php
/**
 * Tạo class Ttvnit_Widget
 */
class Ttvnit_Widget extends WP_Widget {

  /**
   * Thiết lập widget: đặt tên, base ID
   */
  function __construct() {
    parent::__construct(
        'ttvnit_widget',
        __( 'Ttvnit Widget' , 'ttvnit'),
        array( 'description' => __( 'Test how to create a widget') )
    );
  
    if ( is_active_widget( false, false, $this->id_base ) ) {
      add_action( 'wp_head', array( $this, 'css' ) );
    }
  }
  
  function css() {
    ?>
  <style type="text/css">
  .a-stats {
  	width: auto;
  }
  </style>
  <?php
  	}

  /**
   * Tạo form option cho widget
   */
  function form( $instance ) {
    $default = array(
        'link' => 'Youtube link'
    );
    //Gộp các giá trị trong mảng $default vào biến $instance để nó trở thành các giá trị mặc định
    $instance = wp_parse_args( (array) $instance, $default);


    if ( $instance ) {
      $title = $instance['title'];
      $link = $instance['link'];
    }
    else {
      $title = __( 'Title of widget');
      $link = $default['link'];
    }
    ?>
  		<p>
  		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:'); ?></label>
  		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /> <br />
  		<input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" />
  		</p>
  
  <?php
  	}

  	/**
  	 * save widget form
  	 */
  	function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
  		$instance['title'] = strip_tags( $new_instance['title'] );
  		$instance['link'] = strip_tags( $new_instance['link'] );
  		return $instance;
  	}
  	/**
  	 * Show widget
  	 */
  	function widget( $args, $instance ) {
        //extract( $args );
  		echo $args['before_widget'];
  		if ( ! empty( $instance['title'] ) ) {
  			echo $args['before_title'];
  			echo esc_html( $instance['title'] );
  			echo $args['after_title'];
  		}
  		echo $args['after_widget'];
  	}
}