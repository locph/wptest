(function($) {
	$(document).ready(function() {
		if($('#ttvnit_import_benefits input:file').length){
			$('#ttvnit_import_benefits input:file').on('change', function() {
				if($(this).val() != ''){
					var ext = $(this).val().replace(/^.*\./, '');
					if(ext != 'csv'){
						$(this).val('');
						alert('Sorry, wrong format file. Please try again with a csv file.');
					}
		  		}
		  	 });
		}
	});
})(jQuery);
