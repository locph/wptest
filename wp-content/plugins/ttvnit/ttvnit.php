<?php
/**
 * @package Ttvnit
 */
/*
Plugin Name: Johnny Nguyen
Plugin URI: http://ttvnit.com/
Description: Test plugin
Version: 3.0.3
Author: Automattic
Author URI: http://automattic.com/wordpress-plugins/
License: GPLv2 or later
Text Domain: akismet
*/
// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
  echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
  exit;
}
define( 'TTVNIT__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( TTVNIT__PLUGIN_DIR . 'class.ttvnit-widget.php' );

function ttvnit_load_js_file(){
  global $itsec_globals;
  //wp_enqueue_script('ttvnit_js', plugins_url('/js/script.js',__FILE__) );
  wp_enqueue_script( 'ttvnit_js', plugins_url('/js/script.js',__FILE__), array( 'jquery' ), $itsec_globals['plugin_build'] );
  wp_enqueue_style( 'ttvnit_css', plugins_url('/css/style.css',__FILE__));
}
add_action( 'admin_enqueue_scripts','ttvnit_load_js_file');
//add_action('wp_head', 'ttvnit_load_js_file');

function ttvnit_import_menu() {
  //Add menu to level 0
  //add_menu_page('Import benefits',  'Import benefits', 'import', 'import_benefits', 'ttvnit_import_benefits');
  //Add link as submenu
  add_submenu_page( 'tools.php', 'Import benefits',  'Import benefits', 'import', 'import_benefits', 'ttvnit_import_benefits');
  //add_submenu_page( 'index.php', 'Import benefits', 'Import benefits', 'import', 'import_benefits', 'ttvnit_import_benefits');
}
add_action('admin_menu', 'ttvnit_import_menu');
function ttvnit_import_benefits_submit () {
  //Support;Date;Title/Emission;Catégorie
  if (file_exists ( $_FILES['file']['tmp_name'] )) {
    if ($handle = fopen ( $_FILES['file']['tmp_name'], "r" )) {
      if(! feof ( $handle ) ){
        //get header line
        $row = str_getcsv ( fgets ( $handle ), ";" );
      }
      while ( ! feof ( $handle ) ) {
        $row = str_getcsv ( fgets ( $handle ), ";" );
        if(count($row) > 1){
          $cate_id = get_cat_ID( $row[3] ? $row[3] : 'Uncategorized' );
          if (!$cate_id) {
            $cat_name = array('cat_name' =>  $row[3]);
            $cate_id = wp_insert_category($cat_name);
          }        
          if($row[1] != '' && preg_match('/(\d+)\.(\d+)\.(\d+)/', $row[1])){
            $created = preg_replace('/(\d+)\.(\d+)\.(\d+)/', '\3-\2-\1 00:00:00', $row[1]);
          }else{
            $created = date('Y-m-d H:i:s');
          }
          $benefits = array (
              'post_title'    =>  $row[2],
              'post_content'  =>  $row[0],
              'post_category' => array($cate_id),
              'post_date'     => $created,
              'post_date_gmt' => $created,
              'post_author'   => get_current_user_id(),
              'post_type'     => 'benefit',
              'post_status'   => 'publish',
          );
          $post_id = wp_insert_post($benefits);
          if ($post_id) {
            add_post_meta( $post_id, 'custom_date', $created);
          }
        }
      }
    }
    fclose ( $handle );
  }
}

function ttvnit_import_benefits() {
?> 
    <h1 >Importer de retombees </h1>
    <form id="ttvnit_import_benefits" method="post" enctype="multipart/form-data" action="<?php the_permalink();?>" >
      <p>Choisissez votre ficher (.csv)<br />
            <input type="file" name="file" id="file" required><br />
              Format du fichier: Support;Date;Title/Emission;Catégorie
       </p>
       <p><input type="submit" name="import"  value="Import"></p>
    </form>
  <?php
  if (isset($_REQUEST['import'])) {
    global $reg_errors;
    $reg_errors = new WP_Error;
    if ( !empty( $_FILES['file']['tmp_name'] ) && !preg_match('/\.csv$/i', $_FILES['file']['name'])) {
      //wp_check_filetype_and_ext($file, $filename)
      $reg_errors->add('field', 'Sorry, wrong format file. Please try again with a csv file.');
    }
    if ( $reg_errors->get_error_messages() ) {
      foreach ( $reg_errors->get_error_messages() as $error ) {
        echo '<div>';
        echo '<strong>ERROR</strong>:';
        echo $error . '<br/>';
        echo '</div>';
      }
    }else{
          ttvnit_import_benefits_submit();
          echo '<div>';
          echo '<strong>Done</strong>';
          echo '</div>';
    }
  }
}


/*
 * Khởi tạo widget item
*/
if(isset($_GET['test'])){
  add_action( 'widgets_init', 'create_ttvnit_widget' );
  function create_ttvnit_widget() {
    register_widget('Ttvnit_Widget');
  }
}
